<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function dashboard()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }

    public function form()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/form');
        $this->load->view('admin/footer');
    }

    public function table()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/table');
        $this->load->view('admin/footer');
    }

    public function chart()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/chart');
        $this->load->view('admin/footer');
    }

    public function maps()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/maps');
        $this->load->view('admin/footer');
    }

    public function sample()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/sample');
        $this->load->view('admin/footer');
    }

    public function badges()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/badges');
        $this->load->view('admin/footer');
    }

    public function button()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/button');
        $this->load->view('admin/footer');
    }

    public function bread()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/bread');
        $this->load->view('admin/footer');
    }

    public function collapse()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/collapse');
        $this->load->view('admin/footer');
    }

    public function tabs()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/tabs');
        $this->load->view('admin/footer');
    }

    public function typography()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/typography');
        $this->load->view('admin/footer');
    }

    public function icon()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/icon');
        $this->load->view('admin/footer');
    }
}
