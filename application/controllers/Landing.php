<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{
	public function index()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/index');
		$this->load->view('landing/footer');
	}

	public function about()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/about');
		$this->load->view('landing/footer');
	}

	public function courses()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/courses');
		$this->load->view('landing/footer');
	}

	public function courses_detail()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/courses-detail');
		$this->load->view('landing/footer');
	}

	public function elements()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/elements');
		$this->load->view('landing/footer');
	}

	public function blog()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/blog');
		$this->load->view('landing/footer');
	}

	public function single_blog()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/single-blog');
		$this->load->view('landing/footer');
	}

	public function contact()
	{
		$this->load->view('landing/header');
		$this->load->view('landing/contact');
		$this->load->view('landing/footer');
	}
}
