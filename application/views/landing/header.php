<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="icon" href="<?= base_url('assets/img/favicon.png') ?>" type="image/png" />
    <title>SIAKAS</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/flaticon.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/css/themify-icons.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/vendors/owl-carousel/owl.carousel.min.css') ?>" />
    <link rel="stylesheet" href="<?= base_url('assets/vendors/nice-select/css/nice-select.css') ?>" />
    <!-- main css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/frontend.css') ?>" />
</head>

<body>