<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar">
    <div class="navbar-wrapper">
        <div class="navbar-brand header-logo">
            <a href="index.html" class="b-brand">
                <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div>
                <span class="b-title">SIASis</span>
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
        </div>
        <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
                <li class="nav-item pcoded-menu-caption">
                    <label>Navigation</label>
                </li>
                <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item">
                    <a href="<?= base_url('admin/dashboard') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>UI Element</label>
                </li>
                <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu">
                    <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Components</span></a>
                    <ul class="pcoded-submenu">
                        <li class=""><a href="<?= base_url('admin/button') ?>" class="">Button</a></li>
                        <li class=""><a href="<?= base_url('admin/badges') ?>" class="">Badges</a></li>
                        <li class=""><a href="<?= base_url('admin/bread') ?>" class="">Breadcrumb & pagination</a></li>
                        <li class=""><a href="<?= base_url('admin/collapse') ?>" class="">Collapse</a></li>
                        <li class=""><a href="<?= base_url('admin/tabs') ?>" class="">Tabs & pills</a></li>
                        <li class=""><a href="<?= base_url('admin/typography') ?>" class="">Typography</a></li>
                        <li class=""><a href="<?= base_url('admin/icon') ?>" class="">Feather<span class="pcoded-badge label label-danger">NEW</span></a></li>
                    </ul>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Forms & table</label>
                </li>
                <li data-username="form elements advance componant validation masking wizard picker select" class="nav-item">
                    <a href="<?= base_url('admin/form') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Form elements</span></a>
                </li>
                <li data-username="Table bootstrap datatable footable" class="nav-item">
                    <a href="<?= base_url('admin/table') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-server"></i></span><span class="pcoded-mtext">Table</span></a>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Chart & Maps</label>
                </li>
                <li data-username="Charts Morris" class="nav-item"><a href="<?= base_url('admin/chart') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span><span class="pcoded-mtext">Chart</span></a></li>
                <li data-username="Maps Google" class="nav-item active"><a href="<?= base_url('admin/maps') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-map"></i></span><span class="pcoded-mtext">Maps</span></a></li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Pages</label>
                </li>
                <li data-username="Authentication Sign up Sign in reset password Change password Personal information profile settings map form subscribe" class="nav-item pcoded-hasmenu">
                    <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-lock"></i></span><span class="pcoded-mtext">Authentication</span></a>
                    <ul class="pcoded-submenu">
                        <li class=""><a href="<?= base_url('auth/register') ?>" class="" target="_blank">Sign up</a></li>
                        <li class=""><a href="<?= base_url('auth/login') ?>" class="" target="_blank">Sign in</a></li>
                    </ul>
                </li>
                <li data-username="Sample Page" class="nav-item"><a href="<?= base_url('admin/sample') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-sidebar"></i></span><span class="pcoded-mtext">Sample page</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->

<!-- [ Header ] start -->
<header class="navbar pcoded-header navbar-expand-lg navbar-light">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse1" href="javascript:"><span></span></a>
        <a href="index.html" class="b-brand">
            <div class="b-bg">
                <i class="feather icon-trending-up"></i>
            </div>
            <span class="b-title">SIASis</span>
        </a>
    </div>
    <a class="mobile-menu" id="mobile-header" href="javascript:">
        <i class="feather icon-more-horizontal"></i>
    </a>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li><a href="javascript:" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a></li>
            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown">Dropdown</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="javascript:">Action</a></li>
                    <li><a class="dropdown-item" href="javascript:">Another action</a></li>
                    <li><a class="dropdown-item" href="javascript:">Something else here</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <div class="main-search">
                    <div class="input-group">
                        <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                        <a href="javascript:" class="input-group-append search-close">
                            <i class="feather icon-x input-group-text"></i>
                        </a>
                        <span class="input-group-append search-btn btn btn-primary">
                            <i class="feather icon-search input-group-text"></i>
                        </span>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                    <div class="dropdown-menu dropdown-menu-right notification">
                        <div class="noti-head">
                            <h6 class="d-inline-block m-b-0">Notifications</h6>
                            <div class="float-right">
                                <a href="javascript:" class="m-r-10">mark as read</a>
                                <a href="javascript:">clear all</a>
                            </div>
                        </div>
                        <ul class="noti-body">
                            <li class="n-title">
                                <p class="m-b-0">NEW</p>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-1.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>New ticket Added</p>
                                    </div>
                                </div>
                            </li>
                            <li class="n-title">
                                <p class="m-b-0">EARLIER</p>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-2.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>Prchace New Theme and make payment</p>
                                    </div>
                                </div>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-3.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>currently login</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="noti-footer">
                            <a href="javascript:">show all</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown drp-user">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon feather icon-settings"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <img src="<?= base_url('assets/img/user/avatar-1.jpg') ?>" class="img-radius" alt="User-Profile-Image">
                            <span>John Doe</span>
                            <a href="<?= base_url('auth/login') ?>" class="dud-logout" title="Logout">
                                <i class="feather icon-log-out"></i>
                            </a>
                        </div>
                        <ul class="pro-body">
                            <li><a href="javascript:" class="dropdown-item"><i class="feather icon-settings"></i>
                                    Settings</a></li>
                            <li><a href="javascript:" class="dropdown-item"><i class="feather icon-user"></i>
                                    Profile</a></li>
                            <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My
                                    Messages</a></li>
                            <li><a href="<?= base_url('auth/login') ?>" class="dropdown-item"><i class="feather icon-lock"></i>
                                    Lock Screen</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</header>
<!-- [ Header ] end -->

<!-- [ Main Content ] start -->
<section class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Google Map</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="#!">Maps</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Google Maps</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <!-- [ basic-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Basic</h5>
                                        <span class="d-block m-t-5">Map shows places around the world</span>
                                    </div>
                                    <div class="card-block">
                                        <div id="basic-map" class="set-map" style="height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ basic-map ] end -->

                            <!-- [ Markers-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Markers</h5>
                                        <span class="d-block m-t-5">Maps shows <code>location</code> of the place</span>
                                    </div>
                                    <div class="card-block">
                                        <div id="markers-map" class="set-map" style="height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Markers-map ] end -->

                            <!-- [ Geo-Coding-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Geo-Coding</h5>
                                        <span class="d-block m-t-5">Search your location</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form1">
                                            <div class="input-group input-group-button mb-3">
                                                <input type="text" id="address" class="form-control" placeholder="Write your place">
                                                <span class="input-group-addon" id="basic-addon1">
                                                    <button class="btn btn-primary m-0">Search Location</button>
                                                </span>
                                            </div>
                                        </form>
                                        <div id="mapGeo" class="set-map" style="height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Geo-Coding-map ] end -->

                            <!-- [ Overlay-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Overlay</h5>
                                        <span class="d-block m-t-5">Map shows places around the world</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form2">
                                            <div id="mapOverlay" class="set-map" style="height:400px;"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Overlay-map ] end -->

                            <!-- [ Street-View-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Street View</h5>
                                        <span class="d-block m-t-5">Map shows view of street</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form3">
                                            <div id="mapStreet" class="set-map" style="height:400px;"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Street-View-map ] end -->

                            <!-- [ Map-Types-map ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Map Types</h5>
                                        <span class="d-block m-t-5">Select your <code>map-types</code> to see differant views</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form4">
                                            <div id="mapTypes" class="set-map" style="height:400px;"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Map-Types-map ] end -->

                            <!-- [ GeoRSS-Layers ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>GeoRSS Layers</h5>
                                        <span class="d-block m-t-5">Shows <code>RSS</code> location</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form5">
                                            <div id="georssmap" class="set-map" style="height:400px;"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- [ GeoRSS-Layers ] end -->

                            <!-- [ Marker-Clustering ] start -->
                            <div class="col-lg-12 col-xl-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Marker Clustering</h5>
                                        <span class="d-block m-t-5">Multiple markers show differant location</span>
                                    </div>
                                    <div class="card-block">
                                        <form method="post" id="geocoding_form6">
                                            <div id="map" class="set-map" style="height:400px;"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- [ Marker-Clustering ] end -->
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- [ Main Content ] end -->