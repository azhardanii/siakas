<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar">
    <div class="navbar-wrapper">
        <div class="navbar-brand header-logo">
            <a href="index.html" class="b-brand">
                <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div>
                <span class="b-title">SIASis</span>
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
        </div>
        <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
                <li class="nav-item pcoded-menu-caption">
                    <label>Navigation</label>
                </li>
                <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item">
                    <a href="<?= base_url('admin/dashboard') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>UI Element</label>
                </li>
                <li data-username="basic components Button Alert Badges breadcrumb Paggination progress Tooltip popovers Carousel Cards Collapse Tabs pills Modal Grid System Typography Extra Shadows Embeds" class="nav-item pcoded-hasmenu pcoded-trigger active">
                    <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-box"></i></span><span class="pcoded-mtext">Components</span></a>
                    <ul class="pcoded-submenu">
                        <li class="active"><a href="<?= base_url('admin/button') ?>" class="">Button</a></li>
                        <li class=""><a href="<?= base_url('admin/badges') ?>" class="">Badges</a></li>
                        <li class=""><a href="<?= base_url('admin/bread') ?>" class="">Breadcrumb & pagination</a></li>
                        <li class=""><a href="<?= base_url('admin/collapse') ?>" class="">Collapse</a></li>
                        <li class=""><a href="<?= base_url('admin/tabs') ?>" class="">Tabs & pills</a></li>
                        <li class=""><a href="<?= base_url('admin/typography') ?>" class="">Typography</a></li>
                        <li class=""><a href="<?= base_url('admin/icon') ?>" class="">Feather<span class="pcoded-badge label label-danger">NEW</span></a></li>
                    </ul>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Forms & table</label>
                </li>
                <li data-username="form elements advance componant validation masking wizard picker select" class="nav-item">
                    <a href="<?= base_url('admin/form') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Form elements</span></a>
                </li>
                <li data-username="Table bootstrap datatable footable" class="nav-item">
                    <a href="<?= base_url('admin/table') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-server"></i></span><span class="pcoded-mtext">Table</span></a>
                </li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Chart & Maps</label>
                </li>
                <li data-username="Charts Morris" class="nav-item"><a href="<?= base_url('admin/chart') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span><span class="pcoded-mtext">Chart</span></a></li>
                <li data-username="Maps Google" class="nav-item"><a href="<?= base_url('admin/maps') ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-map"></i></span><span class="pcoded-mtext">Maps</span></a></li>
                <li class="nav-item pcoded-menu-caption">
                    <label>Pages</label>
                </li>
                <li data-username="Authentication Sign up Sign in reset password Change password Personal information profile settings map form subscribe" class="nav-item pcoded-hasmenu">
                    <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-lock"></i></span><span class="pcoded-mtext">Authentication</span></a>
                    <ul class="pcoded-submenu">
                        <li class=""><a href="<?= base_url('auth/register') ?>" class="" target="_blank">Sign up</a></li>
                        <li class=""><a href="<?= base_url('auth/login') ?>" class="" target="_blank">Sign in</a></li>
                    </ul>
                </li>
                <li data-username="Sample Page" class="nav-item"><a href="<?= base_url('admin/sample') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-sidebar"></i></span><span class="pcoded-mtext">Sample page</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- [ navigation menu ] end -->

<!-- [ Header ] start -->
<header class="navbar pcoded-header navbar-expand-lg navbar-light">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse1" href="javascript:"><span></span></a>
        <a href="index.html" class="b-brand">
            <div class="b-bg">
                <i class="feather icon-trending-up"></i>
            </div>
            <span class="b-title">SIASis</span>
        </a>
    </div>
    <a class="mobile-menu" id="mobile-header" href="javascript:">
        <i class="feather icon-more-horizontal"></i>
    </a>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li><a href="javascript:" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a></li>
            <li class="nav-item dropdown">
                <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown">Dropdown</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="javascript:">Action</a></li>
                    <li><a class="dropdown-item" href="javascript:">Another action</a></li>
                    <li><a class="dropdown-item" href="javascript:">Something else here</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <div class="main-search">
                    <div class="input-group">
                        <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                        <a href="javascript:" class="input-group-append search-close">
                            <i class="feather icon-x input-group-text"></i>
                        </a>
                        <span class="input-group-append search-btn btn btn-primary">
                            <i class="feather icon-search input-group-text"></i>
                        </span>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown"><i class="icon feather icon-bell"></i></a>
                    <div class="dropdown-menu dropdown-menu-right notification">
                        <div class="noti-head">
                            <h6 class="d-inline-block m-b-0">Notifications</h6>
                            <div class="float-right">
                                <a href="javascript:" class="m-r-10">mark as read</a>
                                <a href="javascript:">clear all</a>
                            </div>
                        </div>
                        <ul class="noti-body">
                            <li class="n-title">
                                <p class="m-b-0">NEW</p>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-1.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>John Doe</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>New ticket Added</p>
                                    </div>
                                </div>
                            </li>
                            <li class="n-title">
                                <p class="m-b-0">EARLIER</p>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-2.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>Joseph William</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>Prchace New Theme and make payment</p>
                                    </div>
                                </div>
                            </li>
                            <li class="notification">
                                <div class="media">
                                    <img class="img-radius" src="<?= base_url('assets/img/user/avatar-3.jpg') ?>" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <p><strong>Sara Soudein</strong><span class="n-time text-muted"><i class="icon feather icon-clock m-r-10"></i>30 min</span></p>
                                        <p>currently login</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="noti-footer">
                            <a href="javascript:">show all</a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown drp-user">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon feather icon-settings"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-notification">
                        <div class="pro-head">
                            <img src="<?= base_url('assets/img/user/avatar-1.jpg') ?>" class="img-radius" alt="User-Profile-Image">
                            <span>John Doe</span>
                            <a href="<?= base_url('auth/login') ?>" class="dud-logout" title="Logout">
                                <i class="feather icon-log-out"></i>
                            </a>
                        </div>
                        <ul class="pro-body">
                            <li><a href="javascript:" class="dropdown-item"><i class="feather icon-settings"></i>
                                    Settings</a></li>
                            <li><a href="javascript:" class="dropdown-item"><i class="feather icon-user"></i>
                                    Profile</a></li>
                            <li><a href="message.html" class="dropdown-item"><i class="feather icon-mail"></i> My
                                    Messages</a></li>
                            <li><a href="<?= base_url('auth/login') ?>" class="dropdown-item"><i class="feather icon-lock"></i>
                                    Lock Screen</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</header>
<!-- [ Header ] end -->

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Button</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Basic Componants</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:">Button</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Default</h5>
                                    </div>
                                    <div class="card-block">
                                        <button type="button" class="btn btn-primary" title="btn btn-primary" data-toggle="tooltip">Primary</button>
                                        <button type="button" class="btn btn-secondary" title="btn btn-secondary" data-toggle="tooltip">Secondary</button>
                                        <button type="button" class="btn btn-success" title="btn btn-success" data-toggle="tooltip">Success</button>
                                        <button type="button" class="btn btn-danger" title="btn btn-danger" data-toggle="tooltip">Danger</button>
                                        <button type="button" class="btn btn-warning" title="btn btn-warning" data-toggle="tooltip">Warning</button>
                                        <button type="button" class="btn btn-info" title="btn btn-info" data-toggle="tooltip">Info</button>
                                        <button type="button" class="btn btn-light" title="btn btn-light" data-toggle="tooltip">Light</button>
                                        <button type="button" class="btn btn-dark" title="btn btn-dark" data-toggle="tooltip">Dark</button>
                                        <button type="button" class="btn btn-link" title="btn btn-link" data-toggle="tooltip">Link</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Outline</h5>
                                    </div>
                                    <div class="card-block">
                                        <button type="button" class="btn btn-outline-primary" title="btn btn-outline-primary" data-toggle="tooltip">Primary</button>
                                        <button type="button" class="btn btn-outline-secondary" title="btn btn-outline-secondary" data-toggle="tooltip">Secondary</button>
                                        <button type="button" class="btn btn-outline-success" title="btn btn-outline-success" data-toggle="tooltip">Success</button>
                                        <button type="button" class="btn btn-outline-danger" title="btn btn-outline-danger" data-toggle="tooltip">Danger</button>
                                        <button type="button" class="btn btn-outline-warning" title="btn btn-outline-warning" data-toggle="tooltip">Warning</button>
                                        <button type="button" class="btn btn-outline-info" title="btn btn-outline-info" data-toggle="tooltip">Info</button>
                                        <button type="button" class="btn btn-outline-light" title="btn btn-outline-light" data-toggle="tooltip">Light</button>
                                        <button type="button" class="btn btn-outline-dark" title="btn btn-outline-dark" data-toggle="tooltip">Dark</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Square Button</h5>
                                    </div>
                                    <div class="card-block">
                                        <p>use <code>.btn-square</code> in class <code>.btn</code> class to get square button</p>
                                        <button type="button" class="btn btn-square btn-primary">Primary</button>
                                        <button type="button" class="btn btn-square btn-secondary">Secondary</button>
                                        <button type="button" class="btn btn-square btn-success">Success</button>
                                        <button type="button" class="btn btn-square btn-danger">Danger</button>
                                        <button type="button" class="btn btn-square btn-warning">Warning</button>
                                        <button type="button" class="btn btn-square btn-info">Info</button>
                                        <button type="button" class="btn btn-square btn-light">Light</button>
                                        <button type="button" class="btn btn-square btn-dark">Dark</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Basic Dropdown Button</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Primary</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Secondary</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Success</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Danger</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-warning dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Warning</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Info</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Split Dropdown Button</h5>
                                    </div>
                                    <div class="card-block">
                                        <!-- Example split danger button -->
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-primary">Primary</button>
                                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-secondary">Secondary</button>
                                            <button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-success">Success</button>
                                            <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-danger">Danger</button>
                                            <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-warning">Warning</button>
                                            <button type="button" class="btn btn-warning dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                        <div class="btn-group mb-2 mr-2">
                                            <button type="button" class="btn btn-info">Info</button>
                                            <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#!">Action</a>
                                                <a class="dropdown-item" href="#!">Another action</a>
                                                <a class="dropdown-item" href="#!">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#!">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->