<!DOCTYPE html>
<html lang="en">

<head>
    <title>SIAKAS</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('assets/img/favicon.png') ?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?= base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css') ?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/animation/css/animate.min.css') ?>">
    <!-- morris css -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/chart-morris/css/morris.css') ?>">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/backend.css') ?>">
</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->